from keras_models.Unet import UEfficientNetB5
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from keras_models.DataGenerator import DataGenerator
from keras.callbacks import ModelCheckpoint
from keras_models.utils import dice_coef
from keras.utils import multi_gpu_model

WIDTH, HEIGHT = 256, 1600
model = UEfficientNetB5(
    input_shape=(WIDTH, HEIGHT, 3), encoder_weights='efficientnet-b5_imagenet_1000_notop.h5',
    final_features=4)

train_df = pd.read_csv('../severstal-steel-defect-detection/train.csv')
train_df['ImageId'] = train_df['ImageId_ClassId'].apply(lambda x: x.split('_')[0])
train_df['ClassId'] = train_df['ImageId_ClassId'].apply(lambda x: x.split('_')[1])
train_df['hasMask'] = ~ train_df['EncodedPixels'].isna()

mask_count_df = train_df.groupby('ImageId').agg(np.sum).reset_index()
mask_count_df.sort_values('hasMask', ascending=False, inplace=True)

non_missing_train_idx = mask_count_df[mask_count_df['hasMask'] > 0]

BATCH_SIZE = 16

train_idx, val_idx = train_test_split(
    non_missing_train_idx.index,  # NOTICE DIFFERENCE
    random_state=2019,
    test_size=0.1
)

train_generator = DataGenerator(
    train_idx,
    df=mask_count_df,
    target_df=train_df,
    batch_size=BATCH_SIZE,
    n_classes=4,
    n_channels=3
)

val_generator = DataGenerator(
    val_idx,
    df=mask_count_df,
    target_df=train_df,
    batch_size=BATCH_SIZE,
    n_classes=4,
    n_channels=3
)

checkpoint = ModelCheckpoint(
    'model_efficientnet.h5',
    monitor='val_loss',
    verbose=0,
    save_best_only=True,
    save_weights_only=False,
    mode='auto'
)

model = multi_gpu_model(model, gpus=2)
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=[dice_coef])
history = model.fit_generator(
    train_generator,
    validation_data=val_generator,
    callbacks=[checkpoint],
    use_multiprocessing=False,
    workers=1,
    epochs=120
)
