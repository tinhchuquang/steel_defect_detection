import cv2
import numpy as np
import pandas as pd
from kaggle import run_length_decode
from torch.utils.data.dataset import Dataset
import copy
import random
from torch.utils.data.sampler import Sampler

DATA_DIR = '/mnt/DATA/tinhcq/data'
PI = np.pi


class Struct(object):
    def __init__(self, is_copy=False, **kwargs):
        self.add(is_copy, **kwargs)

    def add(self, is_copy=False, **kwargs):
        # self.__dict__.update(kwargs)

        if is_copy == False:
            for key, value in kwargs.items():
                setattr(self, key, value)
        else:
            for key, value in kwargs.items():
                try:
                    setattr(self, key, copy.deepcopy(value))
                    # setattr(self, key, value.copy())
                except Exception:
                    setattr(self, key, value)

    def __str__(self):
        text = ''
        for k, v in self.__dict__.items():
            text += '\t%s : %s\n' % (k, str(v))
        return text


def df_loc_by_list(df, key, values):
    df = df.loc[df[key].isin(values)]
    df = df.assign(sort=pd.Categorical(df[key], categories=values, ordered=True))
    df = df.sort_values('sort')
    # df = df.reset_index()
    df = df.drop('sort', axis=1)
    return df


class SteelDataset(Dataset):
    def __init__(self, split, csv, mode, augment=None):

        self.split = split
        self.csv = csv
        self.mode = mode
        self.augment = augment

        self.uid = list(np.concatenate([np.load(DATA_DIR + '/split/%s' % f, allow_pickle=True) for f in split]))
        df = pd.concat([pd.read_csv(DATA_DIR + '/%s' % f) for f in csv])
        df.fillna('', inplace=True)
        df['Class'] = df['ImageId_ClassId'].str[-1].astype(np.int32)
        df['Label'] = (df['EncodedPixels'] != '').astype(np.int32)
        df = df_loc_by_list(df, 'ImageId_ClassId',
                            [u.split('/')[-1] + '_%d' % c for u in self.uid for c in [1, 2, 3, 4]])
        self.df = df

    def __str__(self):
        num1 = (self.df['Class'] == 1).sum()
        num2 = (self.df['Class'] == 2).sum()
        num3 = (self.df['Class'] == 3).sum()
        num4 = (self.df['Class'] == 4).sum()
        pos1 = ((self.df['Class'] == 1) & (self.df['Label'] == 1)).sum()
        pos2 = ((self.df['Class'] == 2) & (self.df['Label'] == 1)).sum()
        pos3 = ((self.df['Class'] == 3) & (self.df['Label'] == 1)).sum()
        pos4 = ((self.df['Class'] == 4) & (self.df['Label'] == 1)).sum()

        length = len(self)
        num = len(self) * 4
        pos = (self.df['Label'] == 1).sum()
        neg = num - pos

        # ---

        string = ''
        string += '\tmode    = %s\n' % self.mode
        string += '\tsplit   = %s\n' % self.split
        string += '\tcsv     = %s\n' % str(self.csv)
        string += '\t\tlen   = %5d\n' % len(self)
        if self.mode == 'train':
            string += '\t\tnum   = %5d\n' % num
            string += '\t\tneg   = %5d  %0.3f\n' % (neg, neg / num)
            string += '\t\tpos   = %5d  %0.3f\n' % (pos, pos / num)
            string += '\t\tpos1  = %5d  %0.3f  %0.3f\n' % (pos1, pos1 / length, pos1 / pos)
            string += '\t\tpos2  = %5d  %0.3f  %0.3f\n' % (pos2, pos2 / length, pos2 / pos)
            string += '\t\tpos3  = %5d  %0.3f  %0.3f\n' % (pos3, pos3 / length, pos3 / pos)
            string += '\t\tpos4  = %5d  %0.3f  %0.3f\n' % (pos4, pos4 / length, pos4 / pos)
        return string

    def __len__(self):
        return len(self.uid)

    def __getitem__(self, index):
        # print(index)
        folder, image_id = self.uid[index].split('/')

        rle = [
            self.df.loc[self.df['ImageId_ClassId'] == image_id + '_1', 'EncodedPixels'].values[0],
            self.df.loc[self.df['ImageId_ClassId'] == image_id + '_2', 'EncodedPixels'].values[0],
            self.df.loc[self.df['ImageId_ClassId'] == image_id + '_3', 'EncodedPixels'].values[0],
            self.df.loc[self.df['ImageId_ClassId'] == image_id + '_4', 'EncodedPixels'].values[0],
        ]
        image = cv2.imread(DATA_DIR + '/%s/%s' % (folder, image_id), cv2.IMREAD_COLOR)
        mask = np.array([run_length_decode(r, height=256, width=1600, fill_value=1) for r in rle])

        infor = Struct(
            index=index,
            folder=folder,
            image_id=image_id,
        )

        if self.augment is None:
            return image, mask, infor
        else:
            return self.augment(image, mask, infor)


def image_to_input(image, rbg_mean, rbg_std):  # , rbg_mean=[0,0,0], rbg_std=[1,1,1]):
    input = image.astype(np.float32)
    input = input[..., ::-1] / 255
    input = input.transpose(0, 3, 1, 2)
    input[:, 0] = (input[:, 0] - rbg_mean[0]) / rbg_std[0]
    input[:, 1] = (input[:, 1] - rbg_mean[1]) / rbg_std[1]
    input[:, 2] = (input[:, 2] - rbg_mean[2]) / rbg_std[2]
    return input


class FourBalanceClassSampler(Sampler):

    def __init__(self, dataset):
        self.dataset = dataset

        label = (self.dataset.df['Label'].values)
        label = label.reshape(-1, 4)
        label = np.hstack([label.sum(1, keepdims=True) == 0, label]).T

        self.neg_index = np.where(label[0])[0]
        self.pos1_index = np.where(label[1])[0]
        self.pos2_index = np.where(label[2])[0]
        self.pos3_index = np.where(label[3])[0]
        self.pos4_index = np.where(label[4])[0]

        # assume we know neg is majority class
        num_neg = len(self.neg_index)
        self.length = 4 * num_neg

    def __iter__(self):
        neg = self.neg_index.copy()
        random.shuffle(neg)
        num_neg = len(self.neg_index)

        pos1 = np.random.choice(self.pos1_index, num_neg, replace=True)
        pos2 = np.random.choice(self.pos2_index, num_neg, replace=True)
        pos3 = np.random.choice(self.pos3_index, num_neg, replace=True)
        pos4 = np.random.choice(self.pos4_index, num_neg, replace=True)

        l = np.stack([neg, pos1, pos2, pos3, pos4]).T
        l = l.reshape(-1)
        return iter(l)

    def __len__(self):
        return self.length


class FixedSampler(Sampler):

    def __init__(self, dataset, index):
        self.dataset = dataset
        self.index = index
        self.length = len(index)

    def __iter__(self):
        return iter(self.index)

    def __len__(self):
        return self.length


def do_random_crop(image, mask, w, h):
    height, width = image.shape[:2]
    x, y = 0, 0
    if width > w:
        x = np.random.choice(width - w)
    if height > h:
        y = np.random.choice(height - h)
    image = image[y:y + h, x:x + w]
    mask = mask[:, y:y + h, x:x + w]
    return image, mask


def do_random_crop_rescale(image, mask, w, h):
    height, width = image.shape[:2]
    x, y = 0, 0
    if width > w:
        x = np.random.choice(width - w)
    if height > h:
        y = np.random.choice(height - h)
    image = image[y:y + h, x:x + w]
    mask = mask[:, y:y + h, x:x + w]

    # ---
    if (w, h) != (width, height):
        image = cv2.resize(image, dsize=(width, height), interpolation=cv2.INTER_LINEAR)

        mask = mask.transpose(1, 2, 0)
        mask = cv2.resize(mask, dsize=(width, height), interpolation=cv2.INTER_NEAREST)
        mask = mask.transpose(2, 0, 1)

    return image, mask


def do_flip_lr(image, mask):
    image = cv2.flip(image, 1)
    mask = mask[:, :, ::-1]
    return image, mask


def do_flip_ud(image, mask):
    image = cv2.flip(image, 0)
    mask = mask[:, ::-1, :]
    return image, mask


def do_random_scale_rotate(image, mask, w, h):
    H, W = image.shape[:2]

    dangle = np.random.uniform(-5, 5)
    dscale = np.random.uniform(-0.15, 0.15, 2)
    dshift = np.random.uniform(0, 1, 2)
    cos = np.cos(dangle / 180 * PI)
    sin = np.sin(dangle / 180 * PI)
    sx, sy = 1 + dscale  # 1,1 #
    tx, ty = dshift

    src = np.array([[-w / 2, -h / 2], [w / 2, -h / 2], [w / 2, h / 2], [-w / 2, h / 2]], np.float32)
    src = src * [sx, sy]
    x = (src * [cos, -sin]).sum(1)
    y = (src * [sin, cos]).sum(1)
    x = x - x.min()
    y = y - y.min()
    x = x + (W - x.max()) * tx
    y = y + (H - y.max()) * ty

    src = np.column_stack([x, y])
    dst = np.array([[0, 0], [w, 0], [w, h], [0, h]])
    s = src.astype(np.float32)
    d = dst.astype(np.float32)
    transform = cv2.getPerspectiveTransform(s, d)

    image = cv2.warpPerspective(image, transform, (w, h),
                                flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(0, 0, 0))

    mask = mask.transpose(1, 2, 0)
    mask = cv2.warpPerspective(mask, transform, (w, h),
                               flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(0, 0, 0, 0))
    mask = mask.transpose(2, 0, 1)
    mask = (mask > 0.5).astype(np.float32)

    return image, mask


def do_random_crop_rotate_rescale(image, mask, w, h):
    H, W = image.shape[:2]

    dangle = np.random.uniform(-8, 8)
    dshift = np.random.uniform(-0.1, 0.1, 2)

    dscale_x = np.random.uniform(-0.00075, 0.00075)
    dscale_y = np.random.uniform(-0.25, 0.25)

    cos = np.cos(dangle / 180 * PI)
    sin = np.sin(dangle / 180 * PI)
    sx, sy = 1 + dscale_x, 1 + dscale_y  # 1,1 #
    tx, ty = dshift * min(H, W)

    src = np.array([[-w / 2, -h / 2], [w / 2, -h / 2], [w / 2, h / 2], [-w / 2, h / 2]], np.float32)
    src = src * [sx, sy]
    x = (src * [cos, -sin]).sum(1) + W / 2
    y = (src * [sin, cos]).sum(1) + H / 2

    src = np.column_stack([x, y])
    dst = np.array([[0, 0], [w, 0], [w, h], [0, h]])
    s = src.astype(np.float32)
    d = dst.astype(np.float32)
    transform = cv2.getPerspectiveTransform(s, d)

    image = cv2.warpPerspective(image, transform, (W, H),
                                flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(0, 0, 0))

    mask = mask.transpose(1, 2, 0)
    mask = cv2.warpPerspective(mask, transform, (W, H),
                               flags=cv2.INTER_NEAREST, borderMode=cv2.BORDER_CONSTANT, borderValue=(0, 0, 0, 0))
    mask = mask.transpose(2, 0, 1)

    return image, mask


def do_random_log_contast(image):
    gain = np.random.uniform(0.70, 1.30, 1)
    inverse = np.random.choice(2, 1)

    image = image.astype(np.float32) / 255
    if inverse == 0:
        image = gain * np.log(image + 1)
    else:
        image = gain * (2 ** image - 1)

    image = np.clip(image * 255, 0, 255).astype(np.uint8)
    return image


def do_noise(image, mask, noise=8):
    H, W = image.shape[:2]
    image = image + np.random.uniform(-1, 1, (H, W, 1)) * noise
    image = np.clip(image, 0, 255).astype(np.uint8)
    return image, mask
