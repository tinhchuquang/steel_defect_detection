import copy
import glob

import cv2
import numpy as np
import torch
import torch.nn as nn
from dataset import image_to_input
from resnet import CONVERSION, PRETRAIN_FILE, load_pretrain, ResNet34
from resnet import IMAGE_RGB_MEAN, IMAGE_RGB_STD
from torch.functional import F
from torch.nn.parameter import Parameter


def gem(x, p=3, eps=1e-6):
    return F.avg_pool2d(x.clamp(min=eps).pow(p), (x.size(-2), x.size(-1))).pow(1. / p)


class GeM(nn.Module):
    def __init__(self, p=3, eps=1e-6):
        super(GeM, self).__init__()
        self.p = Parameter(torch.ones(1) * p).cuda()
        self.eps = eps

    def forward(self, x):
        return gem(x, p=self.p, eps=self.eps)

    def __repr__(self):
        return self.__class__.__name__ + '(' + 'p=' + '{:.4f}'.format(self.p.data.tolist()[0]) + ', ' + 'eps=' + str(
            self.eps) + ')'


class Net(nn.Module):

    def load_pretrain(self, skip, is_print=True):
        conversion = copy.copy(CONVERSION)
        for i in range(0, len(conversion) - 8, 4):
            conversion[i] = 'block.' + conversion[i][5:]
        load_pretrain(self, skip, pretrain_file=PRETRAIN_FILE, conversion=conversion, is_print=is_print)

    def __init__(self, num_class=4, drop_connect_rate=0.2):
        super(Net, self).__init__()

        e = ResNet34()
        self.block = nn.ModuleList([
            e.block0,
            e.block1,
            e.block2,
            e.block3,
            e.block4,
        ])
        e = None  # dropped
        self.feature = nn.Conv2d(512, 32, kernel_size=1)
        self.logit = nn.Conv2d(32, num_class, kernel_size=1)

    def forward(self, x):
        batch_size, C, H, W = x.shape

        for i in range(len(self.block)):
            x = self.block[i](x)
            # print(i, x.shape)

        x = F.dropout(x, 0.5, training=self.training)
        x = GeM()(x)
        x = self.feature(x)
        logit = self.logit(x)
        return logit


def criterion(logit, truth, weight=None):
    batch_size, num_class, H, W = logit.shape
    logit = logit.view(batch_size, num_class)
    truth = truth.view(batch_size, num_class)
    assert (logit.shape == truth.shape)

    loss = F.binary_cross_entropy_with_logits(logit, truth, reduction='none')

    if weight is None:
        loss = loss.mean()

    else:
        pos = (truth > 0.5).float()
        neg = (truth < 0.5).float()
        pos_sum = pos.sum().item() + 1e-12
        neg_sum = neg.sum().item() + 1e-12
        loss = (weight[1] * pos * loss / pos_sum + weight[0] * neg * loss / neg_sum).sum()

    return loss


def metric_hit(logit, truth, thresh=0.5):
    batch_size, num_class, H, W = logit.shape
    with torch.no_grad():
        logit = logit.view(batch_size, num_class, -1)
        truth = truth.view(batch_size, num_class, -1)

        probability = torch.sigmoid(logit)
        p = (probability > thresh).float()
        t = (truth > 0.5).float()

        tp = ((p + t) == 2).float()
        tn = ((p + t) == 0).float()

        tp = tp.sum(dim=[0, 2])
        tn = tn.sum(dim=[0, 2])
        num_pos = t.sum(dim=[0, 2])
        num_neg = batch_size * H * W - num_pos

        tp = tp.data.cpu().numpy()
        tn = tn.data.cpu().numpy().sum()
        num_pos = num_pos.data.cpu().numpy()
        num_neg = num_neg.data.cpu().numpy().sum()

        tp = np.nan_to_num(tp / (num_pos + 1e-12), 0)
        tn = np.nan_to_num(tn / (num_neg + 1e-12), 0)

        tp = list(tp)
        num_pos = list(num_pos)

    return tn, tp, num_neg, num_pos


def make_dummy_data(folder='256x256', batch_size=8):
    image_file = glob.glob('/root/share/project/kaggle/2019/steel/data/dump/%s/image/*.png' % folder)  # 32
    image_file = sorted(image_file)

    input = []
    truth_mask = []
    truth_label = []
    for b in range(0, batch_size):
        i = b % len(image_file)
        image = cv2.imread(image_file[i], cv2.IMREAD_COLOR)
        mask = np.load(image_file[i].replace('/image/', '/mask/').replace('.png', '.npy'))
        label = (mask.reshape(4, -1).sum(1) > 0).astype(np.int32)

        input.append(image)
        truth_mask.append(mask)
        truth_label.append(label)

    input = np.array(input)
    input = image_to_input(input, IMAGE_RGB_MEAN, IMAGE_RGB_STD)

    truth_mask = np.array(truth_mask)
    truth_mask = (truth_mask > 0).astype(np.float32)

    truth_label = np.array(truth_label)

    infor = None

    return input, truth_mask, truth_label, infor
