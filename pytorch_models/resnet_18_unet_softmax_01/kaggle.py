import numpy as np


def run_length_decode(rle, height=256, width=1600, fill_value=1):
    mask = np.zeros((height, width), np.float32)
    if rle != '':
        mask = mask.reshape(-1)
        r = [int(r) for r in rle.split(' ')]
        r = np.array(r).reshape(-1, 2)
        for start, length in r:
            start = start - 1
            mask[start:(start + length)] = fill_value
        mask = mask.reshape(width, height).T
    return mask


